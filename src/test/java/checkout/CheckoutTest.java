package checkout;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator.ReplaceUnderscores;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.HashMap;

import static checkout.Money.fromPence;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DisplayNameGeneration(ReplaceUnderscores.class)
@TestMethodOrder(OrderAnnotation.class)
class CheckoutTest{

  //'@Order' - used for reader in ide not because of an execution order dependency i.e. a dev can easily scan the spec (simple to complex)
  //'ReplaceUnderscores.class' - underscores are easier to read. Also they get replaced with spaces when run anyway.

  private Checkout checkout;

  private Sku skuA = new Sku("A");
  private Sku skuB = new Sku("B");
  private Sku skuC = new Sku("C");
  private Sku skuD = new Sku("D");

  @BeforeEach
  void setUp() {

    // There is a balance to having this setup code here: duplication vs readability.
    // If it were any further from use e.g. 'MultiBuyPromotion' consider refactor
    var itemList = new HashMap<Sku, Item>();
    itemList.put(skuA, new Item(skuA, fromPence(5)));
    itemList.put(skuB, new Item(skuB, fromPence(10)));
    itemList.put(skuC, new Item(skuC, fromPence(15)));
    itemList.put(skuD, new Item(skuD, fromPence(20)));
    var itemCatalogue = new ItemCatalogue(itemList);

    var promotionA = new MultiBuyPromotion(skuA, 3, fromPence(5));
    var promotionB = new MultiBuyPromotion(skuB, 2, fromPence(5));
    var promotions = new PromotionList(asList(promotionA, promotionB));

    checkout = new Checkout(itemCatalogue, promotions);
  }

  @Test
  @Order(1)
  void new_checkout_has_zero_total() {
    assertThat(checkout.total()).isEqualTo(fromPence(0));
  }

  @ParameterizedTest
  @CsvSource({"'A',       5",
              "'A,B',     15",
              "'A,B,C',   30",
              "'A,B,C,D', 50"})
  @Order(2)
  void scanning_an_item_adds_its_price_to_the_total(String skusString, int expectedTotal) {

    scanItems(skusString);

    assertThat(checkout.total()).isEqualTo(fromPence(expectedTotal));
  }

  @Test
  @Order(3)
  void scanning_an_invalid_sku_throws_InvalidSkuException() {

    var invalidSku = new Sku("invalid-sku");

    assertThatThrownBy(() -> checkout.scan(invalidSku))
        .isInstanceOf(InvalidSkuException.class)
        .hasMessage("No item for sku: invalid-sku");
  }

  @Nested
  class Scanning_an_item_on_multi_buy_discount {

    @Test
    @Order(4)
    void does_not_apply_any_discount_if_threshold_not_met() {

      checkout.scan(skuA)
              .scan(skuA);

      assertThat(checkout.total()).isEqualTo(fromPence(10));
    }

    @ParameterizedTest
    @CsvSource({"'A,A,A',         10",
                "'A,A,A,A',       15",
                "'A,A,A,A,A,A',   20",
                "'B,B',           15",
                "'B,B,B',         25",
                "'B,B,B,B',       30",
                "'A,B,A,B,A,C,D', 60"})
    @Order(5)
    void applies_discount_if_item_quantity_threshold_is_met(String skusString, int expectedTotal) {

      scanItems(skusString);

      assertThat(checkout.total()).isEqualTo(fromPence(expectedTotal));
    }
  }

  private void scanItems(String skusString) {
    Arrays.stream(skusString.split(","))
        .forEach(sku -> checkout.scan(new Sku(sku)));
  }
}
