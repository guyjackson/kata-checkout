package checkout;

class NoPromotion extends MultiBuyPromotion {

  NoPromotion() {
    super(new Sku(""), 1, Money.fromPence(0));
  }
}
