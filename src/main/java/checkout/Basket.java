package checkout;

import java.util.HashMap;
import java.util.Map;

class Basket {

  private Map<Sku, BasketItemLine> basketItemLines = new HashMap<>();
  private PromotionList promotionList;

  Basket(PromotionList promotionList) {
    this.promotionList = promotionList;
  }

  void add(Item item) {
    basketItemLines.merge(item.sku(), new BasketItemLine(item, 1),
        (v1, v2) -> new BasketItemLine(v1.item(), v1.quantity() + 1));
  }

  Money total() {
    var total = calculateTotal();

    var discounts = calculateBasketDiscount();

    return total.subtract(discounts);
  }

  private Money calculateTotal() {
    var result = Money.fromPence(0);

    for (var basketItemLine : basketItemLines.values()) {

      result = result.add(basketItemLine.total());
    }

    return result;
  }

  private Money calculateBasketDiscount() {
    var result = Money.fromPence(0);

    for (var basketItemLine : basketItemLines.values()) {

      var discount = calculateBasketItemLineDiscount(basketItemLine);

      result = result.add(discount);
    }

    return result;
  }

  private Money calculateBasketItemLineDiscount(BasketItemLine basketItemLine) {
    var promotion = promotionList.getPromotion(basketItemLine.item().sku());

    var discountQuantityEntitlement = basketItemLine.quantity() / promotion.getThresholdQuantity();

    return promotion.getDiscount().multiply(discountQuantityEntitlement);
  }
}
