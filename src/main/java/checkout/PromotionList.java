package checkout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class PromotionList {

  private Map<Sku, MultiBuyPromotion> promotions = new HashMap<>();

  PromotionList(List<MultiBuyPromotion> multiBuyPromotions) {
    multiBuyPromotions.forEach(p -> this.promotions.put(p.getSku(), p));
  }

  MultiBuyPromotion getPromotion(Sku sku) {
    return Optional.ofNullable(promotions.get(sku))
        .orElse(new NoPromotion());
  }
}
