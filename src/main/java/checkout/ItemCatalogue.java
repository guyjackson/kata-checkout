package checkout;

import java.util.Map;
import java.util.Optional;

class ItemCatalogue {

  private Map<Sku, Item> itemList;

  ItemCatalogue(Map<Sku, Item> itemList) {
    this.itemList = itemList;
  }

  Item getItem(Sku sku) {
    return Optional.ofNullable(itemList.get(sku))
        .orElseThrow(() -> new InvalidSkuException("No item for sku: " + sku.sku()));
  }
}
