package checkout;

class InvalidSkuException extends RuntimeException {

  InvalidSkuException(String message) {
    super(message);
  }
}
