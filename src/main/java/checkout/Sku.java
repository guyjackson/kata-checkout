package checkout;

public class Sku {

  private String id;

  Sku(String id) {
    this.id = id;
  }

  String sku() {
    return id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Sku sku1 = (Sku) o;

    return id != null ? id.equals(sku1.id) : sku1.id == null;
  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}
