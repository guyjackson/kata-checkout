package checkout;

public class Item {

  private Money price;
  private Sku sku;

  public Item(Sku sku, Money price) {
    this.sku = sku;
    this.price = price;
  }

  public Item(Sku sku) {
    this.sku = sku;
  }

  Money price() {
    return price;
  }

  Sku sku() {
    return sku;
  }
}
