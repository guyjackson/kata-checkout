package checkout;

public class Checkout {

  private ItemCatalogue itemCatalogue;
  private Basket basket;

  public Checkout(ItemCatalogue itemCatalogue, PromotionList promotionList) {
    this.itemCatalogue = itemCatalogue;
    this.basket = new Basket(promotionList);
  }

  Money total() {
    return basket.total();
  }

  Checkout scan(Sku sku) {
    var item = itemCatalogue.getItem(sku);
    basket.add(item);
    return this;
  }
}
