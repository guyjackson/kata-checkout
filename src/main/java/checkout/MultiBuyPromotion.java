package checkout;

class MultiBuyPromotion {

  private final Sku sku;
  private final int thresholdQuantity;
  private final Money discount;

  MultiBuyPromotion(Sku sku, int thresholdQuantity, Money discount) {
    this.sku = sku;
    this.thresholdQuantity = thresholdQuantity;
    this.discount = discount;
  }

  Sku getSku() {
    return sku;
  }

  int getThresholdQuantity() {
    return thresholdQuantity;
  }

  Money getDiscount() {
    return discount;
  }
}
