package checkout;

public class Money {

  private int pence;

  private Money(int pence) {
    this.pence = pence;
  }

  static Money fromPence(int pence) {
    return new Money(pence);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Money money = (Money) o;

    return pence == money.pence;
  }

  @Override
  public int hashCode() {
    return pence;
  }

  Money add(Money otherMoney) {
    return fromPence(this.pence + otherMoney.pence);
  }

  Money subtract(Money otherMoney) {
    return fromPence(this.pence - otherMoney.pence);
  }

  Money multiply(int multiple) {
    return fromPence(this.pence * multiple);
  }

  @Override
  public String toString() {
    return "Money{" +
        "pence=" + pence +
        '}';
  }
}
