package checkout;

class BasketItemLine {

  private Item item;
  private int quantity;

  BasketItemLine(Item item, int quantity) {

    this.item = item;
    this.quantity = quantity;
  }

  Money total(){
    return item.price().multiply(quantity);
  }

  int quantity() {
    return quantity;
  }

  Item item() {
    return item;
  }
}
