# Shopping Checkout Kata

<b>The Kata</b>

Implement the code for a supermarket checkout that calculates the total price of a number of items. Goods are priced individually, however there are weekly special offers for when multiple items are bought. For example “Apples are 50 each or 3 for 130″.

<table>
  <tr>
    <th>Item</th>
    <th>Price</th>
    <th>Offer</th>
  </tr>
  <tr>
    <td>A</td>
    <td>5</td>       
    <td>3 for 10</td>
  </tr>
  <tr>
    <td>B</td>     
    <td>10</td>      
    <td>2 for 15</td>
    </tr>
  <tr>
    <td>C</td>     
    <td>15</td>
    <td></td>
    </tr>
  <tr>
    <td>D</td>    
    <td>20</td>
    <td></td>
  </tr>
</table>


